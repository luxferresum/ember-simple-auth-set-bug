import Ember from 'ember';

export default Ember.Controller.extend({
	session: Ember.inject.service(),
	actions:{
		withSessionSet() {
			let session = Ember.get(this, 'session');
			session.set('data.with-session-set', true);
		},
		withEmberSet() {
			let session = Ember.get(this, 'session');
			Ember.set(session, 'data.with-ember-set', true);
		}
	}
});